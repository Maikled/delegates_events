﻿using System;
using Timer.Implementation;
using Timer.Interfaces;

namespace Timer.Factories
{
	public class CountDownNotifierFactory
	{
		public ICountDownNotifier CreateNotifierForTimer(Timer timer)
		{
			if (timer == null)
				throw new ArgumentNullException("Timer not can be null", nameof(timer));

			return new CountDownNotifier(timer);
		}
	}
}
