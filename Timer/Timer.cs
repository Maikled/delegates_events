﻿using System;
using System.Threading;

namespace Timer
{
	public class Timer
	{
		public event Action<string, int> OnStarted = delegate { };
		public event Action<string, int> OnTick = delegate { };
		public event Action<string> OnStopped = delegate { };

		private string _name;
		private int _clock;

		public Timer(string name, int clock)
        {
			if (name == null || name == "")
				throw new ArgumentException("Argument is not valid", nameof(name));
			_name = name;

			if (clock <= 0)
				throw new ArgumentException("Argument is not valid", nameof(clock));
			_clock = clock;
        }

		public void TimerStart()
        {
			OnStarted?.Invoke(_name, _clock);
        }
		public void TimerTick()
        {
			while (_clock > 1)
            {
				_clock -= 1;
				OnTick?.Invoke(_name, _clock);
			}
        }
		public void TimerStop()
        {
			OnStopped?.Invoke(_name);
        }
	}
}