﻿using System;
using Timer.Interfaces;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		private Timer _timer;

		public CountDownNotifier(Timer timer)
        {
			_timer = timer;
        }

		public void Init(Action<string, int> startDelegate, Action<string> stopDelegate, Action<string, int> tickDelegate)
		{
			_timer.OnStarted += startDelegate;
			_timer.OnStopped += stopDelegate;
			_timer.OnTick += tickDelegate;
		}

		public void Run()
		{
			_timer.TimerStart();
			_timer.TimerTick();
			_timer.TimerStop();
		}
	}
}